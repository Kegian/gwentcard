PROJECT?=frontend

COMPOSE_CMD=docker-compose

run:
	$(COMPOSE_CMD) up

run-detach:
	$(COMPOSE_CMD) up -d

build:
	$(COMPOSE_CMD) exec $(PROJECT) yarn build

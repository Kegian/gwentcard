    export function processedText(description: any): string {
        if (description === undefined) {
            return '';
        }
        return description.replace(/\[/g, '<b>').replace(/\]/g, '</b>');
    }
